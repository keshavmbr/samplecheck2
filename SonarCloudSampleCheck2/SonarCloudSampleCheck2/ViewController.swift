//
//  ViewController.swift
//  SonarCloudSampleCheck2
//
//  Created by Keshav MB on 25/04/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.sonarCloudSample2CheckProject()
    }

    func sonarCloudSample2CheckProject() {
        print("Sonar cloud sample2 project changes")
        self.sonarCloudSampleSubBranchCreated()
        
    }

    func sonarCloudSampleSubBranchCreated() {
        print("Sub branch creatd")
        
    }
    
}
